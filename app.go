package main

import (
	"os"
	"time"

	"github.com/urfave/cli/v2"
	log "gitlab.com/omid.mt/golog"
)

func main() {
	log.Infoln("Starting dools")

	app := &cli.App{
		Name:                 "Dools",
		Version:              "v0.0.1",
		Compiled:             time.Now(),
		Usage:                "Create virtual drive using multiple cloud drive service",
		EnableBashCompletion: true,
		HideHelp:             false,
		HideVersion:          false,
		Flags: []cli.Flag{
			&cli.BoolFlag{Name: "verbose", Aliases: []string{"vvv"}, Usage: "Enable verbose mode with more logging"},
			&cli.PathFlag{Name: "config", Aliases: []string{"c"}, Usage: "Config file path"},
		},
		Commands: []*cli.Command{
			{
				Name:     "add",
				Category: "drive",
			},
			{
				Name:     "remove",
				Category: "drive",
			},
			{
				Name:     "mount",
				Category: "drive",
			},
			{
				Name:     "unmount",
				Category: "drive",
			},
			{
				Name:  "passwd",
				Usage: "Set master password",
			},
		},
	}

	app.Run(os.Args)
}
