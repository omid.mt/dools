module dools

go 1.15

replace github.com/omidmt/grypt => ../grypts

require (
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/omid.mt/golog v1.0.0
)
